#! /usr/bin/env python2.7
from pylab import *
from lfm import *
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    ### Generate matrix factorization from best final model
    rating = genfromtxt('data.txt', dtype = int)
    M, N, K, L = 943, 1682, 20, rating.shape[0]
    umat, vmat, train_error, test_error = LatentFactorModel(M, N, K, rating, beta = 18., verbose = True, max_step = 10, detrend = True)

    ### (1) Remove mean movies from U and V
    vmat_mean = np.mean(vmat, axis = 0)

    for i in range(N):
        vmat[i,:] = vmat[i,:] - vmat_mean

    for i in range(M):
        umat[i,:] = umat[i,:] - vmat_mean

    ### (2) Get SVD for V to get an orthogonal base based on movie features
    A, S, B = np.linalg.svd(vmat.T, full_matrices=True)

    print S

    # Get low-rank approximation of U and V using transform defined by A
    u_lr = np.dot(A[:,[0,1]].T, umat.T)
    v_lr = np.dot(A[:,[0,1]].T, vmat.T)

    # Rescale movie and user vectors by their respective standard deviations (for plotting)
    for i in range(2):
        u_lr[i,:] = u_lr[i,:]/np.std(u_lr[i,:])
        v_lr[i,:] = v_lr[i,:]/np.std(v_lr[i,:])

    ### (4) Get movie names and genres (without the unknown genre)
    movies = np.loadtxt('movies.txt', dtype=str, delimiter='\t')
    genres = movies[:,3:].astype(int)
    names = movies[:,1]

    ### (6) Fig1: Define approximate movie index vector Yisong used in his online example
    y_ind = [24, 26, 27, 81, 484, 34, 77, 456, 1, 28, 6, 178, 134, 10, 99, 171, 180, 49, 21, 68]

    for mov in y_ind:
        plt.plot(v_lr[0,mov], v_lr[1,mov], 'mo')
        plt.text(v_lr[0,mov]+0.05, v_lr[1,mov], names[mov], fontsize=6)

    plt.plot([-5.5, 5.5], [0, 0], 'k--')
    plt.plot([0, 0], [-5.5, 5.5], 'k--')
    plt.ylim(-2.5, 5.5)
    plt.xlim(-5.5, 5.5)

    plt.savefig('fig1.eps')
    plt.close()

    ### (7) Fig2: Plot a few movie series
    y_ind = np.array([[49, 180, 171],
             [28, 230, 253, 402],
             [70, 94, 421, 0, 224, 992, 541],
             [126, 186],
             [134, 178, 473, 187]])

    clrs = ['r', 'g', 'm', 'c', 'b']

    leg_handle = []

    for i in range(5):
        tmp, = plt.plot(v_lr[0,y_ind[i]], v_lr[1,y_ind[i]], 'o', color=clrs[i], markersize=12,  alpha = 0.5)
        leg_handle.append(tmp)

    plt.plot([-5.5, 5.5], [0, 0], 'k--')
    plt.plot([0, 0], [-5.5, 5.5], 'k--')
    plt.ylim(-2, 6)
    plt.xlim(-5.5, 5.5)

    plt.legend(leg_handle, ['Star Wars IV-VI', 'Batman', 'Disney Movies', 'Godfather I+II', 'Kubrick Movies'],
               numpoints=1, loc='lower left', fontsize=10)

    plt.savefig('fig2.eps')
    plt.close()

    ### (8) Fig3: Plot the average genre of movies with known genre
    leg_handle = []

    for i in range(18):
        ind = (genres[:,i] == 1).nonzero()[0]
        tmp, = plt.plot(np.mean(v_lr[0,ind]), np.mean(v_lr[1,ind]), 'o', color=np.random.rand(3), markersize=10,  alpha = 0.5)
        leg_handle.append(tmp)

    plt.plot([-5.5, 5.5], [0, 0], 'k--')
    plt.plot([0, 0], [-5.5, 5.5], 'k--')
    plt.ylim(-1, 1)
    plt.xlim(-1, 1.5)

    plt.legend(leg_handle, ['Action', 'Adventure', 'Animation', 'Children', 'Comedy', 'Crime',
                            'Documentary', 'Drama', 'Fantasy', 'Film-Noir', 'Horror', 'Musical',
                            'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western'], numpoints=1,
               fontsize=10, loc='center right')

    plt.savefig('fig3.eps')
    plt.close()

    ### (9) Fig4: Histogram plot for two genres
    sel_genre = [3, 9]
    sel_name = ['Comedy', 'Film-Noir']

    for i in range(2):
        ind = (genres[:,sel_genre[i]] == 1).nonzero()[0]
        n, bins, patches = plt.hist(v_lr[0,ind], 15, normed=1, histtype='stepfilled', label=sel_name[i])
        plt.setp(patches, 'facecolor', np.random.rand(3), 'alpha', 0.5)

    plt.legend(loc='upper left')

    plt.savefig('fig4.eps')
    plt.close()