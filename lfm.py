#! /usr/bin/env python2.7
from pylab import *
import operator

def LatentFactorModel(M, N, K, train, test = None, beta = 18., verbose = False, max_step = 240, detrend = True):
    umat = rand(M, K)
    vmat = rand(N, K)
    ymat_train = zeros((M, N))
    ymat_test = zeros((M, N))
    id_train, id_test = [], []
    mean_u, mean_v, mean_uv = zeros(M), zeros(N), zeros((M, N))
    num_u, num_v = zeros(M), zeros(N)

    step = 0

    # initialization
    for i in range(train.shape[0]):
        id = ravel_multi_index((train[i, 0] - 1, train[i, 1] - 1), (M, N))
        ymat_train[train[i, 0] - 1, train[i, 1] - 1] = train[i, 2]
        id_train.append(id)
    mean_ymat_train = sum(ymat_train) / len(id_train)

    if detrend:
        for r in range(len(id_train)):
            i, j = unravel_index(id_train[r], (M, N))
            ymat_train[i, j] -= mean_ymat_train

        for r in range(len(id_train)):
            i, j = unravel_index(id_train[r], (M, N))
            mean_u[i] += ymat_train[i, j]
            num_u[i] += 1
            mean_v[j] += ymat_train[i, j]
            num_v[j] += 1

        for i in range(M):
            if num_u[i] != 0.: mean_u[i] /= num_u[i]
            else: mean_u[i] = 0.
        for j in range(N):
            if num_v[j] != 0.: mean_v[j] /= num_v[j]
            else: mean_v[j] = 0.

        for i in range(M):
            for j in range(N):
                mean_uv[i, j] = mean_ymat_train + mean_u[i] + mean_v[j]
        for r in range(len(id_train)):
            i, j = unravel_index(id_train[r], (M, N))
            ymat_train[i, j] -= mean_u[i] + mean_v[j]

    approx = dot(umat, vmat.T) + mean_uv
    error = 0
    for r in range(len(id_train)):
        i, j = unravel_index(id_train[r], (M, N))
        error += (ymat_train[i, j] - approx[i, j]) * (ymat_train[i, j] - approx[i, j])
    error = sqrt(error / len(id_train))
    if verbose: print 'init_train_error = %f' % error

    if test != None:
        for i in range(test.shape[0]):
            id = ravel_multi_index((test[i, 0] - 1, test[i, 1] - 1), (M, N))
            ymat_test[test[i, 0] - 1, test[i, 1] - 1] = test[i, 2]
            id_test.append(id)

        error = 0
        for r in range(len(id_test)):
            i, j = unravel_index(id_test[r], (M, N))
            error += (ymat_test[i, j] - approx[i, j]) * (ymat_test[i, j] - approx[i, j])
        error = sqrt(error / len(id_test))
        if verbose: print 'init_test_error = %f' % error

    # training
    while 1:
        step += 1

        rhs_u = zeros((M, K))
        lhs_u = zeros((M, K, K))
        for r in range(len(id_train)):
            i, j = unravel_index(id_train[r], (M, N))
            rhs_u[i] += ymat_train[i, j] * vmat[j]
            lhs_u[i] += dot(vmat[j].reshape(K, 1), vmat[j].reshape(1, K))
        for i in range(M):
            umat[i] = 2. * solve(beta * eye(K) + 2. * lhs_u[i], rhs_u[i])

        rhs_v = zeros((N, K))
        lhs_v = zeros((N, K, K))
        for r in range(len(id_train)):
            i, j = unravel_index(id_train[r], (M, N))
            rhs_v[j] += ymat_train[i, j] * umat[i]
            lhs_v[j] += dot(umat[i].reshape(K, 1), umat[i].reshape(1, K))
        for j in range(N):
            vmat[j] = 2. * solve(beta * eye(K) + 2. * lhs_v[j], rhs_v[j])

        score, train_error = 0, 0
        approx = dot(umat, vmat.T)
        for r in range(len(id_train)):
            i, j = unravel_index(id_train[r], (M, N))
            score += (ymat_train[i, j] - approx[i, j]) * (ymat_train[i, j] - approx[i, j])
        train_error = sqrt(score / len(id_train))
        score += beta / 2. * (norm(umat, 'fro') + norm(vmat, 'fro'))

        if verbose: print 'step = %d, score = %f, training_error = %f' % (step, score, train_error)

        if (step > max_step): break

    # testing
    approx = dot(umat, vmat.T) + mean_uv
    test_error = 0
    if test != None:
        for r in range(len(id_test)):
            i, j = unravel_index(id_test[r], (M, N))
            test_error += (ymat_test[i, j] - approx[i, j]) * (ymat_test[i, j] - approx[i, j])
        test_error = sqrt(test_error / len(id_test))
        if verbose: print 'testing_error = %f' % test_error
        
    return umat, vmat, train_error, test_error

def cross_validation(beta, fold, verbose = False):
    rating = genfromtxt('data.txt', dtype = int)
    M, N, K, L = 943, 1682, 20, rating.shape[0]

    train_index, test_index = [], []
    train_error_list, test_error_list = [], []
    sub = L / fold
    for i in range(fold):
        test_index.append(range(i * sub, (i + 1) * sub))
    for i in range(5):
        print 'cross validation set #%d:' % i
        suit = [j for j in range(fold) if j != i]
        train_index = reduce(operator.add, [test_index[j] for j in suit])
        umat, vmat, train_error, test_error = LatentFactorModel(M, N, K, rating[train_index], rating[test_index[i]], 
                beta = beta, max_step = 240, verbose = True, detrend = True)
        train_error_list.append(train_error)
        test_error_list.append(test_error)
        print 'train_error = %f, test_error = %f' % (train_error, test_error)

    print 'avg_train_error = %f, avg_test_error = %f' % (mean(train_error), mean(test_error))

if __name__ == '__main__':
    # for cross validation
    #for beta in [2.**i for i in range(-1, 8)]:
    for beta in [20.]:
        print 'beta = %f' % beta
        cross_validation(beta, 5)

    # final model
    #rating = genfromtxt('data.txt', dtype = int)
    #M, N, K, L = 943, 1682, 20, rating.shape[0]
    #umat, vmat, train_error, test_error = LatentFactorModel(M, N, K, rating, beta = 10., verbose = True)
