#! /usr/bin/env python2.7
from pylab import *

def LatentFactorModel(M, N, K, rating, alpha = 1., beta = 1.):
    L = rating.shape[0]

    Umat = ones((M, K))
    Vmat = ones((N, K))
    #Umat = rand(M, K)
    #Vmat = rand(N, K)

    Ymat = zeros((M, N))
    step, max_step = 0, 1000

    for i in range(L):
        Ymat[rating[i, 0] - 1, rating[i, 1] - 1] = rating[i, 2]
    #Ymat = array([[5,3,0,1],[4,0,0,1],[1,1,0,5],[1,0,0,4],[0,1,5,4],])
    print Ymat

    while 1:
        print step
        step += 1
        for i in range(M):
            for j in range(N):
                if Ymat[i, j] > 0:
                    eij = Ymat[i, j] - dot(Umat[i, :], Vmat[j, :])
                    for k in range(K):
                        Umat[i, k] -= alpha * (beta * Umat[i, k] - 2. * eij * Vmat[j, k])
                        Vmat[j, k] -= alpha * (beta * Vmat[j, k] - 2. * eij * Umat[i, k])
        error = 0
        for i in range(M):
            for j in range(N):
                if Ymat[i, j] > 0:
                    error += pow((Ymat[i, j] - dot(Umat[i, :], Vmat[j, :])), 2)
                    for k in range(K):
                        error += beta / 2 * (pow(Umat[i, k], 2) + pow(Vmat[j, k], 2))
        if error < 0.001 or step > max_step : break
        #if step % 10 == 0: print step

    return Umat, Vmat

if __name__ == '__main__':
    rating = genfromtxt('data.txt', dtype = int)
    M, N, K = 943, 1682, 20
    Umat, Vmat = LatentFactorModel(M, N, K, rating, alpha = 0.0002, beta = 0.02)
